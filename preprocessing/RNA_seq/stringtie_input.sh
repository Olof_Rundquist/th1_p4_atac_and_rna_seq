#!/bin/bash -l

for i in ./*Aligned.sortedByCoord.out.bam
do
    sbatch stringtie_run.sh $i
done
