#!/bin/bash -l

#SBATCH -J Stringtie
#SBATCH -n 32
#SBATCH -N 1
#SBATCH -t 00:30:00

bam=$1

stringtie -eB -p 32 -G /proj/lassim/reference_genomes_OR/human/hg38_Ensemble/Homo_sapiens.GRCh38.90.gtf -o ${bam%%.Aligned.sortedByCoord.out.bam}/expression.gtf $bam
