#!/bin/bash -l

np_TH1=$(find ./np_TH1_raw/*RNA -prune -type d)
prog_TH1=$(find ./prog_TH1_raw/*p -prune -type d)

for i in $np_TH1
do
    name=${i##./np_TH1_raw/}
    f=$(find ${i}/*_1.fq.gz -type f -print0 | tr "\0" ",")
    r=$(find ${i}/*_2.fq.gz -type f -print0 | tr "\0" ",")
    sbatch star_PE.sh "${f}" "${r}" np_${name}
done


for i in $prog_TH1
do
    name=${i##./prog_TH1_raw/}
    f=$(find ${i}/*/*_1.fq.gz -type f)
    r=$(find ${i}/*/*_2.fq.gz -type f)
    sbatch star_PE.sh "${f}" "${r}" prog_${name}
done


