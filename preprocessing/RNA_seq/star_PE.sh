#!/bin/bash -l

#SBATCH -J STAR
#SBATCH -n 16
#SBATCH --mem=94000 
#SBATCH -t 04:00:00

cd ../data/fastq

forward=${1}
reverse=${2}
genomedir=/proj/lassim/reference_genomes_OR/human/hg38_Ensemble/star_index
outname=${3}

STAR --runThreadN 16 --outSAMtype BAM SortedByCoordinate --outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical --readFilesCommand zcat --genomeDir $genomedir --readFilesIn ${forward} ${reverse} --outTmpDir ${outname}.tmp --outFileNamePrefix ${outname}.

sbatch stringtie_run.sh ${outname}.Aligned.sortedByCoord.out.bam
