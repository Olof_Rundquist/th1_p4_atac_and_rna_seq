#!/bin/bash -l

#SBATCH -n 32
#SBATCH -t 02:00:00

samtools merge --threads 32 $2 $1
samtools index $2

