## RNA-Seq preprocessing

Here I will walkthrough the preprocessing steps carried out on the RNA-seq.

### Quality control
Sequencing data quality checks were carried out using FastQC. See the fastqc.sh script in the quality controll directory.
A summary of the quality report for all read files is avilable as a multiqc report in said directory. This report also lists the alignment rate for all samples. As all samples were of good quality and no adapter content was detected we proceeded immedietly to aligning the reads using STAR.

### Sequencing read alignment and Gene quantification
Sequencing read alignment was carried out using STAR as outlined in the "star_PE.sh" script.
Gene expression was subsequently quantified using stringtie as outlined in the "stringtie_run.sh" script and the results summarized into Gene and transcript count tables using the PrepDE.py script provided from the stringtie manual http://ccb.jhu.edu/software/stringtie/index.shtml?t=manual. 

