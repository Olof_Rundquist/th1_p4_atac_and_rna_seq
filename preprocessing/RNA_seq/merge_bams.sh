#!/bin/bash -l

#Define the groups
np_TH1_0h=$(find np_R*-0m*Aligned.sortedByCoord.out.bam)
np_TH1_05h=$(find np_R*-30m*Aligned.sortedByCoord.out.bam)
np_TH1_1h=$(find np_R*-1h*Aligned.sortedByCoord.out.bam)
np_TH1_2h=$(find np_R*-2h*Aligned.sortedByCoord.out.bam)
np_TH1_6h=$(find np_R*-6h*Aligned.sortedByCoord.out.bam)
np_TH1_24h=$(find np_R*-24h*Aligned.sortedByCoord.out.bam)

prog_TH1_0h=$(find prog_*0p*Aligned.sortedByCoord.out.bam)
prog_TH1_05h=$(find prog_*0.5p*Aligned.sortedByCoord.out.bam)
prog_TH1_1h=$(find prog_*1p*Aligned.sortedByCoord.out.bam)
prog_TH1_2h=$(find prog_*2p*Aligned.sortedByCoord.out.bam)
prog_TH1_6h=$(find prog_*6p*Aligned.sortedByCoord.out.bam)
prog_TH1_24h=$(find prog_*24p*Aligned.sortedByCoord.out.bam)

l=($(compgen -v | grep TH1_))
for index in ${!l[*]}
do
    sbatch bam_merge.sh "${!l[$index]}" ${l[$index]}.merged.bam
done

