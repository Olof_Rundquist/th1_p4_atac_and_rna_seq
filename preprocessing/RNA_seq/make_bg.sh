#!/bin/bash -l

#SBATCH -t 03:00:00

bedtools genomecov -bg -ibam $1 > ${1%%.bam}.bg
