#!/bin/bash -l

#SBATCH -J shif_alignment
#SBATCH -n 32
#SBATCH -t 04:00:00

#Shift the bam file for read counting etc.
#This is in the python 3.6 env. 
source activate anaconda_36

alignmentSieve -p max --ATACshift -b ${1} -o ${1%%.bam}.shifted.bam

source activate NGS_env
sbatch sort_bam_postion.sh ${1%%.bam}.shifted.bam

