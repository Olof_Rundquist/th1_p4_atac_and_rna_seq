#!/bin/bash -l

#Find all of the sample directories
samples=$(find ../data/P*/P13* -prune -type d)

#give the files for each smaple to trim galore. 
#Trim galore expects input on the form of forrward reverses forward reverse ...
#Simply find all files for each sample and give them as a group. Comma delimit the list and split it in the trim galore script.
mkdir ../data/trimmed_galore
for i in $samples
do
    files=$(find $i/*/*/*.fastq.gz -type f)
    sbatch trim_galore.sh "${files}" ${i##../*/} # ${i##../*/} = sample name 
done
# "${files}" sends the whole space delimted list as one argument (a string)


