#!/bin/bash -l

#SBATCH -t 04:00:00
#SBATCH -n 32
#SBATCH -J Trim_galore

files=${1} # space delimted list of input files
name=${2}

mkdir ../data/trimmed_galore/${name}
trim_galore -o ../data/trimmed_galore/${name} --nextera --fastqc --paired --retain_unpaired ${files} 
