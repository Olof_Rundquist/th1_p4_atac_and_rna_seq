#!/bin/bash -l

#Define the groups
np_TH1_0h=$(find ../data/aligned/P13114_10{01,13,19}*genrich_peaks.bed)
np_TH1_05h=$(find ../data/aligned/P13114_10{02,14,20}*genrich_peaks.bed)
np_TH1_1h=$(find ../data/aligned/P13114_10{03,15,21}*genrich_peaks.bed)
np_TH1_2h=$(find ../data/aligned/P13114_10{04,16,22}*genrich_peaks.bed)
np_TH1_6h=$(find ../data/aligned/P13114_10{05,17,23}*genrich_peaks.bed)
np_TH1_24h=$(find ../data/aligned/P13114_10{06,18,24}*genrich_peaks.bed)

prog_TH1_0h=$(find ../data/aligned/P13114_10{07,25,31}*genrich_peaks.bed)
prog_TH1_05h=$(find ../data/aligned/P13114_10{08,26,32}*genrich_peaks.bed)
prog_TH1_1h=$(find ../data/aligned/P13114_10{09,27,33}*genrich_peaks.bed)
prog_TH1_2h=$(find ../data/aligned/P13114_10{10,28,34}*genrich_peaks.bed)
prog_TH1_6h=$(find ../data/aligned/P13114_10{11,29,35}*genrich_peaks.bed)
prog_TH1_24h=$(find ../data/aligned/P13114_10{12,30,36}*genrich_peaks.bed)

#Intersect the files
#Get the loaded varibels as a list by using comgen and grep
l=($(compgen -v | grep TH1_))
for index in ${!l[*]}
do
    bedtools multiinter -i ${!l[$index]} > ../peak_filtering/${l[$index]}.multiintersect.bed
done
 
python Filter_peaks.py 


for i in ../peak_filtering/*consensus.bed
do 
    bedtools sort -i $i | bedtools merge -d 150 > ${i%%.bed}.merged.bed #Merges all peaks with less than 150bp (=read_length) between each other
done

#And finally take the union of the files
merged=$(find ../results/*merged.bed)

cat $merged | bedtools sort | bedtools merge -d 150 > ../peak_filtering/Final_Consensus_peaks.bed


 
