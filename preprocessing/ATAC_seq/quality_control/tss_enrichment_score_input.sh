#!/bin/bash -l

mkdir ../data/aligned/tss_scores
for i in ../data/aligned/*shifted.bam
do
    sbatch tss_enrichment_score.sh $i
done
