#!/bin/bash -l

#SBATCH -t 04:00:00
#SBATCH -n 2
#SBATCH --mem=12000

f=${1##../*/}

tssenrich --genome hg38 --memory 3 --processes 2 --log ../data/aligned/tss_scores/${f%%.*}.log.txt $1 > ../data/aligned/tss_scores/${f%%.*}.tss_score.txt
