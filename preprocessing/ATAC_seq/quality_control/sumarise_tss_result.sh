#!/bin/bash -l

touch All_scores.txt
for i in *tss_score.txt 
do
    name=${i%%.*} # get sample name
    value=$(cat $i) # Get value
    echo $name $value >> All_scores.txt # Append to file
done
    
