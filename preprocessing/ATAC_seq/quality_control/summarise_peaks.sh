#!/bin/bash -l

touch ../quality_controll/Number_of_peaks.txt
for i in ../data/aligned/*genrich*bed 
do
    file_name=${i##../*/}
    name=${file_name%%.*} # get sample name
    value=$(wc -l $i) # Get value
    echo $name $value >> ../quality_controll/Number_of_peaks.txt # Append to file
done
    
