#!/bin/bash -l

mkdir ../data/aligned/fragment_sizes
for i in ../data/aligned/*sorted*rmdup*bam
do
    sbatch get_fragment_size.sh $i
done

