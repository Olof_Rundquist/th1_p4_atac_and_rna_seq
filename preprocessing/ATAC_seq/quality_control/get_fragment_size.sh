#!/bin/bash -l

#SBATCH -n 1
#SBATCH -t 04:00:00

out=${1##../data/aligned/}

samtools view $1 | awk '$9>0' | cut -f 9 | sort | uniq -c | sort -b -k2,2n | sed -e 's/^[ \t]*//' > ../data/aligned/fragment_sizes/${out%%bam}fragment_length_count.txt 

