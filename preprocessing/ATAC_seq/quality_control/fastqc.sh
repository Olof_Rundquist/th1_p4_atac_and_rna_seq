#!/bin/bash -l

#SBATCH -J fastqc
#SBATCH -t 04:00:00
#SBATCH -n 32

source deactivate
source activate NGS_env

#make the output directory for the reports
mkdir ../quality_controll/fastqc_logs

#$2 is $ATAC-seq_files/*fastq.gz 
#$1 is the output dir
fastqc -t 200 -o ../quality_controll/fastqc_logs ../data/*/*fq  

#cd into output directory and run multiqc.
cd ../quality_controll/fastqc_logs

multiqc *zip
