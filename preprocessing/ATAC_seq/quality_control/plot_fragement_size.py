#!/usr/bin/env python

import numpy as np
from matplotlib import pyplot as plt
from os import listdir

#Test lines
#ma = np.loadtxt("P15A_Tys-12.sorted.rmdup.filtered.fragment_length_count.txt", dtype="int")
#plt.xlim(0, 1500) ; plt.plot(ma[:,1], ma[:,0])
#plt.semilogy() ; plt.xlim(0, 1500) ; plt.plot(ma[:,1], ma[:,0])

#For loop
for i in [x for x in listdir() if "fragment_length_count" in x]: # Get all Fragment length count files
    ma = np.loadtxt(i, dtype="int") # Load in as 2d numpy array
    sample_name=i.split("_")[0] # Get the sample name
    plt.xlim(0, 1500) ; plt.plot(ma[:,1], ma[:,0]) ; plt.savefig("./plots/" + sample_name + "_linear_fragment_size.png") ; plt.close() # plot Linear graph
    plt.semilogy() ; plt.xlim(0, 1500) ; plt.plot(ma[:,1], ma[:,0]) ; plt.savefig("./plots/" + sample_name + "_log_fragment_size.png") ; plt.close() # plot log graph
    
#Done

