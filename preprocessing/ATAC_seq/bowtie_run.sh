#!/bin/bash

#SBATCH -J bowtie2
#SBATCH -t 24:00:00
#SBATCH -N 1
#SBATCH -n 32

forward=$(echo ${1} | tr " " ",") # Creates comma delimited list of forward read files
reverse=$(echo ${2} | tr " " ",") # Creates comma delimited list of reverse read files
unpaired=$(echo ${3} | tr " " ",") # Creates comma delimited list of unpaired read files
ref_index=${5} # Reference genome.
out=${4}

bowtie2 -p 32 -k 10 --very-sensitive -x $ref_index -1 ${forward} -2 ${reverse} -U ${unpaired} --met-file ${out}.bowtie.metrics.txt | samtools view -b -@ 32 | samtools sort -n -@ 32 -m 2500M > ${out}.Aligned.sorted_by_readname.bam

#For expression analysis
samtools sort -@ 32 -m 2500M ${out}.Aligned.sorted_by_readname.bam > ${out}.Aligned.sorted_by_postion.bam
samtools index ${out}.Aligned.sorted_by_postion.bam

sbatch picard_remove_dups.sh ${out}.Aligned.sorted_by_postion.bam
sbatch genrich_run.sh ${out}.Aligned.sorted_by_readname.bam



