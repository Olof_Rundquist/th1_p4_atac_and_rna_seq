#!/bin/bash -l

#SBATCH -J Genrich
#SBATCH -n 1
#SBATCH -t 08:00:00
#SBATCH --mem=40000 # 40gb Uses like 20-35gb

bam=$1
out=${bam%%.bam}.genrich_peaks.bed

Genrich -t $bam -o $out -j -y -r -v -e chrM 
