#!/bin/bash -l

#Define the groups
np_TH1_0h=$(find ../data/aligned/P13114_10{01,13,19}*sorted_by_postion*shifted.sorted.bam)
np_TH1_05h=$(find ../data/aligned/P13114_10{02,14,20}*sorted_by_postion*shifted.sorted.bam)
np_TH1_1h=$(find ../data/aligned/P13114_10{03,15,21}*sorted_by_postion*shifted.sorted.bam)
np_TH1_2h=$(find ../data/aligned/P13114_10{04,16,22}*sorted_by_postion*shifted.sorted.bam)
np_TH1_6h=$(find ../data/aligned/P13114_10{05,17,23}*sorted_by_postion*shifted.sorted.bam)
np_TH1_24h=$(find ../data/aligned/P13114_10{06,18,24}*sorted_by_postion*shifted.sorted.bam)

prog_TH1_0h=$(find ../data/aligned/P13114_10{07,25,31}*sorted_by_postion*shifted.sorted.bam)
prog_TH1_05h=$(find ../data/aligned/P13114_10{08,26,32}*sorted_by_postion*shifted.sorted.bam)
prog_TH1_1h=$(find ../data/aligned/P13114_10{09,27,33}*sorted_by_postion*shifted.sorted.bam)
prog_TH1_2h=$(find ../data/aligned/P13114_10{10,28,34}*sorted_by_postion*shifted.sorted.bam)
prog_TH1_6h=$(find ../data/aligned/P13114_10{11,29,35}*sorted_by_postion*shifted.sorted.bam)
prog_TH1_24h=$(find ../data/aligned/P13114_10{12,30,36}*sorted_by_postion*shifted.sorted.bam)

l=($(compgen -v | grep TH1_))
for index in ${!l[*]}
do
    sbatch bam_merge.sh "${!l[$index]}" ../data/aligned/${l[$index]}.merged.shifted.bam
done

