#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 24 16:06:01 2019

@author: x_oloru
"""

import pandas as pd
from sys import argv
from os import listdir

#First run bedtools multiniter across all files in X group.
for i in [x for x in listdir("../results/") if "multiintersect.bed" in x]:
    df=pd.read_table("../results/" + i, header=None)
    #Drop unecessary columns
    df=df.iloc[:, 0:4]
    #Number of samples
    sample_number=3
    #Drop areas not supported by all samples in time point
    df=df[df[3] >= 2]
    #Done
    df.to_csv("../results/" + i.split("/")[-1].split(".")[0] + ".consensus.bed", index=None, header=None, sep="\t")
