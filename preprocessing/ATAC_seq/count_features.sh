#!/bin/bash
#SBATCH -J FeatureCounts # A single job name for the array
#SBATCH -N 1 # Nodes
#SBATCH -n 32 # Processes
#SBATCH -t 12:00:00 # 1 h
#Activate the conda enviroment. 
#source activate NGS_env

files=$(find ../data/aligned/*Aligned*shifted*bam -type f)

awk 'OFS="\t" {print $1"."$2+1"."$3, $1, $2+1, $3, "."}' $1 > ${1%%.bed}.saf

annotation=${1%%.bed}.saf
output=${1%%.bed}.count_matrix_tsv

featureCounts -p -T 32 -F 'SAF' -a ${annotation} -o ${output} ${files} 2> ../results/Counting_log.txt 
