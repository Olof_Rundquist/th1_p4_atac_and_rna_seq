#!/bin/bash -l

#SBATCH -J Picard-rmdup
#SBATCH -n 2
#SBATCH -t 08:00:00

#For unknow reasons samtools truncate the file when removing duplicates.
#Therfore we are now using picard instead.
bam=${1}
picard MarkDuplicates I=${bam} M=${bam%%.bam}.duplicate.metrics.txt O=${bam%%.bam}.rmdup.bam REMOVE_DUPLICATES=true

samtools index ${bam%%.bam}.rmdup.bam

sbatch shif_alignment.sh ${bam%%.bam}.rmdup.bam
