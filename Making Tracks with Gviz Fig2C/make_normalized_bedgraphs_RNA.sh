#!/bin/bash -l

#SBATCH -J bamCoverage
#SBATCH -n 16
#SBATCH -t 00:45:00
#SBATCH --mem=85000

bamCoverage -p max --normalizeUsing RPKM -bs 10 -of bedgraph -b $1 -o ${1%%.bam}.RPKM.bg
bamCoverage -p max --normalizeUsing RPKM -bs 10 -of bigwig -b $1 -o ${1%%.bam}.RPKM.bw
