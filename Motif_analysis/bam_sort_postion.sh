#!/bin/bash

#SBATCH -J sort_bam
#SBATCH -t 06:00:00
#SBATCH -N 1
#SBATCH -n 32

samtools sort -@ 32 -m 2500M ${1} > ${1%%.bam}.sorted.bam
samtools index ${1%%.bam}.sorted.bam

sbatch wellington_fimo_homer.sh Final_Consensus_peaks_120.bed ${1%%.bam}.sorted.bam
sbatch wellington_fimo_homer.sh Core_promtor_peaks_120.bed ${1%%.bam}.sorted.bam

