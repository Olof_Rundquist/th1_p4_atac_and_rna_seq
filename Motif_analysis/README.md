### Motif Analysis
This directory outlines the base motif analysis performed for the ATAC-seq.

The called peaks, footprints and matched motifs from the study are provided in their entirty so the results can be replicated.
If you desire to rerun evrything from the original fastq read files please see the data preprocessing instructions. They outline how to produce the provided files.

Two motif analysis runs were performed. 
The first focused on all peaks to get an overview of TF dynamics during Th1 differnetiation and is in the paper presented as figure 2D and E.
For this footprint were merged for each time point from both time series and a motif enrichment carried out using homer with the control as background.

The analysis setup for this is presented in "Fig2_All_peaks".
To recreate the tabels used in the R analysis (see the R script):
   1. Run "motif_analysis_homer.sh" 
   2. Then run the "Summarize_Time.py" script in the Motif_enrichment directory to summarize the results.

The second motif analysis focused on the P4 response peaks (the intersection in figure 3A and B) to focus on the reulation of P4 affected chromatin.
The analysis setup for this is presented in "Fig3__P4_response_peaks".
To recreate the tabels used in the R analysis (see the R script):
   1. Run "filter_footprints.sh" to filter the footprint files to only the footprints present in the P4 response peaks.
   2. Then run "motif_analysis_homer_launch.sh" to perform the motif enrichment for all promoter and distal peaks for each time point. 
   3. Finally run the "Summarize_Time.py" script in the Motif_enrichment directory to summarize the results.

