#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 15:45:44 2021

@author: x_oloru
"""

import pandas as pd
import os

# Read in the footprints
Fp = pd.read_table("np_TH1_05h.merged.sorted.bam.Final_Consensus_peaks_120.bed.WellingtonFootprints.FDR.0.01.bed", header=None)
Fp.columns = ["chr", "start", "end", "PositionID", "fp_logP", "strand"]

# Read in the mapped motifs
motifs = pd.read_table("./wellington_motifs/Mapped_motifs.bed", header=0)

# And merge. Well taht was easy.
Fp.merge(motifs, on="PositionID")


# And now run over all.
dirs = os.listdir("./bams")
dirs=[x for x in dirs if x.endswith("_wellington_footprints")]

for i in dirs:
    f=os.listdir("./bams/" + i)
    f=[x for x in f if x.endswith("WellingtonFootprints.FDR.0.01.bed")][0]
    Fp = pd.read_table("./bams/" + i + "/" + f, header=None)
    Fp.columns = ["chr", "start", "end", "PositionID", "fp_logP", "Fp_Strand"]
    Fp.to_csv("./bams/" + i.replace("merged.sorted_wellington_footprints", "wellington_footprints.bed"), index=None, sep="\t")
    motifs = pd.read_table("./bams/" + i + "/wellington_motifs/Mapped_motifs.bed")
    motifs.rename(columns={"Strand" : "Motif_Strand"}, inplace=True)
    Fp = Fp.merge(motifs, on="PositionID")
    Fp.to_csv("./bams/" + i.replace("merged.sorted_wellington_footprints", "wellington_mapped_footprints.tsv"), index=None, sep="\t")
