#!/bin/bash
#SBATCH -J wellington # A single job name for the array
#SBATCH -N 1 # Nodes
#SBATCH -n 32 # Processes
#SBATCH -t 48:00:00 # 48 h 
#SBATCH -o footprinting-%A-%a.out # Standard output
#SBATCH -e footprinting-%A-%a.err # Standard error

#This script needs a diffrent env since pyDNase is not fully upgraded to python3
source activate pydnase

bed=${1} #Peak file. Minimum peak size is 120! # Note this was empiricaliy tested by feeding wellington different peak sizes after some mysterious crashes.
bam=${2} #bam file, Note no read shifting necessary since -A does that.
out=${bam%%.bam}_wellington_footprints # output directory.

#Run the wellington
mkdir -p ${out} # Make the output directory
wellington_footprints.py -p 0 ${bed} ${bam} ${out} -sh 7,36,1 -fp 6,41,1 -A -fdr 0.01 -fdriter 100 -fdrlimit -30



