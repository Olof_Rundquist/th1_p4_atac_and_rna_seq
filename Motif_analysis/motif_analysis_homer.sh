#!/bin/bash -l
#SBATCH -J Homer # A single job name for the array
#SBATCH -n 1 # Processes
#SBATCH --mem=40000 # RAM
#SBATCH -t 02:00:00 # 1 h
#SBATCH -o Motif_homer-%A-%a.out # Standard output
#SBATCH -e Motif_homer-%A-%a.err # Standard error

#mkdir -p ${1%%.bam}_wellington_footprints/wellington_motifs/
#findMotifsGenome.pl ${1%%.bam}_wellington_footprints/*FDR.0.01.bed hg38 ${1%%.bam}_wellington_footprints/wellington_motifs/ -size given -preparse

# Also output the motif mapping
#findMotifsGenome.pl ${1%%.bam}_wellington_footprints/*FDR.0.01.bed hg38 ${1%%.bam}_wellington_footprints/wellington_motifs/ -size given -find homer.custom.motifs.txt > ${1%%.bam}_wellington_footprints/wellington_motifs/Mapped_motifs.bed

# make the background file.
#bedtools shuffle -i ${1%%.bam}_wellington_footprints/*FDR.0.01_core_peaks.bed -incl All_Promtors_fix.bed -g hg38.chrom.sizes | bedtools sort > ${1%%.bam}_wellington_footprints/Core_peaks.bg.bed
# And run
mkdir -p ${1}/wellington_motifs_core/
findMotifsGenome.pl ${1}/*FDR.0.01_core_peaks.bed hg38 ${1}/wellington_motifs_core/ -size given -preparse -bg ./bams/Footprint_background.bed 

mkdir -p ${1}/wellington_motifs_core_prog/
findMotifsGenome.pl ${1}/*FDR.0.01_core_peaks_prog.bed hg38 ${1}/wellington_motifs_core_prog/ -size given -preparse -bg ./bams/Footprint_background.bed 
