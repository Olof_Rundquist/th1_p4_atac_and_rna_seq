#!/bin/bash -l

for bam in ./bams/*Aligned.sorted_by_postion.rmdup.bam
do
    sbatch wellington_run.sh Final_Consensus_peaks_120.bed $bam
done
