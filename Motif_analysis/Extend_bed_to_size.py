#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 16:00:46 2020

@author: x_oloru
"""

import sys
import pandas as pd

bed=pd.read_table(sys.argv[1], header=None)
min_size=int(sys.argv[2])
#bed=pd.read_table("Core_promtor_peaks.bed", header=None)
bed.columns=["Seqnames", "start", "end"]

for i in bed.index:
    if (bed.iloc[i].end - bed.iloc[i].start) <= min_size:
        ex=min_size - (bed.iloc[i].end - bed.iloc[i].start) 
        bed.loc[i,"start"]= bed.iloc[i].start - int(ex/2)
        bed.loc[i, "end"] = bed.iloc[i].end + int(ex/2)
    else :
        next

bed.to_csv(sys.argv[1].split(".")[0] + "_" + sys.argv[2] + ".bed", sep="\t", header=None, index=None)

