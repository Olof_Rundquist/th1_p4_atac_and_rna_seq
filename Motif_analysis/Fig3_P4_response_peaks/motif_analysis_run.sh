#!/bin/bash -l
#SBATCH -J Homer # A single job name for the array
#SBATCH -n 1 # Processes
#SBATCH -t 01:00:00 # 1 h
#SBATCH -o Motif_homer-%A-%a.out # Standard output
#SBATCH -e Motif_homer-%A-%a.err # Standard error

in=$1
out=$2

# Then each time point.
# Promoter
findMotifsGenome.pl $in hg38 $out -size given -preparse -bg ../footprints/merge/Control.bed -keepOverlappingBg
