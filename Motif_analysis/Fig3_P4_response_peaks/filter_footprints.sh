#!/bin/bash -l

mkdir -p ./Filterd_footprints/Promoter/
mkdir -p ./Filterd_footprints/Distal/
# The basic
for i in '05' '1' '2' '6' '24' 
do
   bedtools intersect -wa -a ../footprints/merge/Time${i}.bed -b ./peaks/Promoter/${i}h.bed > ./Filterd_footprints/Promoter/${i}h.bed 
   bedtools intersect -wa -a ../footprints/merge/Time${i}.bed -b ./peaks/Distal/${i}h.bed > ./Filterd_footprints/Distal/${i}h.bed 
done

bash motif_analysis_homer_launch.sh 


