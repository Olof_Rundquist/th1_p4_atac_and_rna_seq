#!/bin/bash -l

mkdir -p ./Motif_enrichment/Promoter
mkdir -p ./Motif_enrichment/Distal

# For each time point.
for i in '05' '1' '2' '6' '24' 
do
    mkdir -p ./Motif_enrichment/Promoter/Time${i}
    mkdir -p ./Motif_enrichment/Distal/Time${i}
    sbatch motif_analysis_run.sh ./Filterd_footprints/Promoter/${i}h.bed ./Motif_enrichment/Promoter/Time${i}
    sbatch motif_analysis_run.sh ./Filterd_footprints/Distal/${i}h.bed ./Motif_enrichment/Distal/Time${i}
done

