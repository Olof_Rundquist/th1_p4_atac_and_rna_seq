#!/bin/bash -l
#SBATCH -J Homer # A single job name for the array
#SBATCH -n 1 # Processes
#SBATCH -t 04:00:00 # 1 h
#SBATCH -o Motif_homer-%A-%a.out # Standard output
#SBATCH -e Motif_homer-%A-%a.err # Standard error

source activate pydnase
mkdir -p ./Motif_analysis

# Then each time point.
mkdir -p ./Motif_analysis/Time05
findMotifsGenome.pl ../footprints/merge/Time05.bed hg38 ./Motif_analysis/Time05 -size given -preparse -bg ../footprints/merge/Control.bed -keepOverlappingBg

mkdir -p ./Motif_analysis/Time1
findMotifsGenome.pl ../footprints/merge/Time1.bed hg38 ./Motif_analysis/Time1 -size given -preparse -bg ../footprints/merge/Control.bed -keepOverlappingBg

mkdir -p ./Motif_analysis/Time2
findMotifsGenome.pl ../footprints/merge/Time2.bed hg38 ./Motif_analysis/Time2 -size given -preparse -bg ../footprints/merge/Control.bed -keepOverlappingBg

mkdir -p ./Motif_analysis/Time6
findMotifsGenome.pl ../footprints/merge/Time6.bed hg38 ./Motif_analysis/Time6 -size given -preparse -bg ../footprints/merge/Control.bed -keepOverlappingBg

mkdir -p ./Motif_analysis/Time24
findMotifsGenome.pl ../footprints/merge/Time24.bed hg38 ./Motif_analysis/Time24 -size given -preparse -bg ../footprints/merge/Control.bed -keepOverlappingBg
