#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 17 16:19:32 2021

@author: x_oloru
"""

import pandas as pd
from scipy import stats
import os
import numpy as np
import decimal

dirs = os.listdir(".")
dirs=[x for x in dirs if x.startswith("Time")]
dirs=[x for x in dirs if not "v" in x]
dirs=[dirs[x] for x in [3,2,1,4,0]]

# load in all of the enrichment tabels.
count=0
for i in dirs:
    if count == 0:
        df =  pd.read_table(i + "/knownResults.txt", sep="\t")
        df['Total Motifs in ' + i] = int(df.columns[5].split('(of ')[1][:-1])
        df['Total Motifs in Control ' + i] = int(df.columns[7].split('(of ')[1][:-1])
        # Rename the columns for ease of reading
        df = df.rename(columns = {
                'P-value' : 'P-value ' + i,
                'Log P-value' : 'Log P-value ' + i,
                'q-value (Benjamini)' : 'q-value (Benjamini) ' + i,
                '# of Target Sequences with Motif(of 29394)' : 'Frequency in ' + i, 
                '% of Target Sequences with Motif' : 'Precentage in ' + i,
                '# of Background Sequences with Motif(of 29165)' : 'Frequency in Control ' + i,
                '% of Background Sequences with Motif' : 'Precentage in Control ' + i})
        # Convert precentage to numeric
        df['Precentage in ' + i] = df.loc[:,df.columns.str.startswith('Precentage in ' + i)].replace("%", "", regex=True).astype("float")
        df['Precentage in Control ' +i] = df.loc[:,df.columns.str.startswith('Precentage in Control ' + i)].replace("%", "", regex=True).astype("float")
        count+=1
    else: 
        nf =  pd.read_table(i + "/knownResults.txt", sep="\t")
        nf['Total Motifs in ' + i] = int(nf.columns[5].split('(of ')[1][:-1])
        nf['Total Motifs in Control ' + i] = int(nf.columns[7].split('(of ')[1][:-1])
        # Rename the columns for ease of reading
        nf = nf.rename(columns = {
                'P-value' : 'P-value ' + i,
                'Log P-value' : 'Log P-value ' + i,
                'q-value (Benjamini)' : 'q-value (Benjamini) ' + i,
                '# of Target Sequences with Motif(of ' + str(nf['Total Motifs in ' + i][1])+ ')' : 'Frequency in ' + i, 
                '% of Target Sequences with Motif' : 'Precentage in ' + i,
                '# of Background Sequences with Motif(of '+ str(nf['Total Motifs in Control ' + i][1]) +')' : 'Frequency in Control ' + i,
                '% of Background Sequences with Motif' : 'Precentage in Control ' + i})
        # Convert precentage to numeric
        nf['Precentage in ' + i] = nf.loc[:,nf.columns.str.startswith('Precentage in ' + i)].replace("%", "", regex=True).astype("float")
        nf['Precentage in Control ' + i] = nf.loc[:,nf.columns.str.startswith('Precentage in Control ' + i)].replace("%", "", regex=True).astype("float")
        count+=1
        df = pd.merge(df, nf, on=["Motif Name", "Consensus"])
# Make all of the contorl columns into anverages. Drop the non average columns.
df["Total Motifs in Control"] = df[[x for x in df.columns if "Total Motifs in Control " in x]].mean(axis=1)
df["Frequency in Control"] = df[[x for x in df.columns if "Frequency in Control " in x]].mean(axis=1)
df["Precentage in Control"] = df[[x for x in df.columns if "Precentage in Control " in x]].mean(axis=1)

df.drop([x for x in df.columns if "Total Motifs in Control " in x], inplace=True, axis=1)
df.drop([x for x in df.columns if "Frequency in Control " in x], inplace=True, axis=1)
df.drop([x for x in df.columns if "Precentage in Control " in x], inplace=True, axis=1)

# Sort the columns for ease of use.  
df = df.reindex(columns=list(df.columns[0:2]) + sorted(df.columns[2:]))
# Drop duplicate rows (For some reason one of the TFs duplicates itself 2^x per merge)
df=df.drop_duplicates()
df.index = range(0, len(df))

# And save the table.
df.to_csv("Motif_enrichment_Time_stats.tsv", index=False)
