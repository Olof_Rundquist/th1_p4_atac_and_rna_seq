#!/bin/bash -l

Samples=($(find ../*wellington_footprints.bed))

# First the control
touch control.tmp.bed
for i in 1 7
do
    tail -n +2 ${Samples[$i]} >> control.tmp.bed
done

# Sort and merge  
bedtools sort -i control.tmp.bed | bedtools merge > control.bed


# Make one for all
touch All.tmp.bed
for i in 1 2 3 4 5 6 7 8 9 10 11 
do
    tail -n +2 ${Samples[$i]} >> All.tmp.bed
done

# Sort and merge
bedtools sort -i All.tmp.bed | bedtools merge > All.bed 



# Also make one for each time point
# 05
touch Time05.tmp.bed
for i in 0 6
do
    tail -n +2 ${Samples[$i]} >> Time05.tmp.bed
done

# Sort 
bedtools sort -i Time05.tmp.bed  | bedtools merge >  Time05.bed 

# 1
touch Time1.tmp.bed
for i in 2 8
do
    tail -n +2 ${Samples[$i]} >> Time1.tmp.bed
done

# Sort 
bedtools sort -i Time1.tmp.bed | bedtools merge > Time1.bed 

# 2
touch Time2.tmp.bed
for i in 4 10
do
    tail -n +2 ${Samples[$i]} >> Time2.tmp.bed
done

# Sort 
bedtools sort -i Time2.tmp.bed | bedtools merge > Time2.bed 

# 6
touch Time6.tmp.bed
for i in 5 11
do
    tail -n +2 ${Samples[$i]} >> Time6.tmp.bed
done

# Sort 
bedtools sort -i Time6.tmp.bed | bedtools merge > Time6.bed 

# 24
touch Time24.tmp.bed
for i in 3 9
do
    tail -n +2 ${Samples[$i]} >> Time24.tmp.bed
done

# Sort 
bedtools sort -i Time24.tmp.bed  | bedtools merge > Time24.bed 


